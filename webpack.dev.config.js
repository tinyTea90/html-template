const path = require('path');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const webpackBase = require('./webpack.config');
const apiMoker = require('webpack-api-mocker');

console.log('development enviroment, data from mock');

module.exports = webpackMerge.merge(webpackBase, {
  devtool: 'eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
      'process.env.BASE_API': JSON.stringify('/mock')
    })
  ],
  devServer: {
    before(app) {
      apiMoker(app, path.resolve('./mock/index.js'));
    }
  }
});
