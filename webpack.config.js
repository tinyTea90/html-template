// 该文件其实最终是要在node环境下执行的
const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const pages = require('./views/index');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// 导出一个具有特殊属性配置的对象
module.exports = {
  // entry: './views/home/index.js' /* 入口文件模块路径 */,
  entry: () => {
    const entrys = {};
    for (const page of pages) {
      entrys[page.pageId] = page.entryPath;
    }
    return entrys;
  },
  output: {
    path: path.join(
      __dirname,
      './dist/'
    ) /* 出口文件模块所属目录，必须是一个绝对路径 */,
    filename: 'src/[name]_bundle_[hash].js' /* 打包的结果文件名称 */
  },
  devServer: {
    // 配置webpack-dev-server的www目录
    contentBase: path.join(__dirname, 'dist'),
    port: 8888,
    index: 'home.html'
  },
  // plugins: [
  //   // 该插件可以把index.html打包到bundle.js文件所属目录，跟着bundle走
  //   // 同时也会自动在index.html中注入script引用链接，并且引用的资源名称，也取决于打包的文件名称
  //   new htmlWebpackPlugin({
  //     template: './views/home/index.html'
  //   })
  // ],
  plugins: [
    new CleanWebpackPlugin(),
    ...pages.map((page) => {
      return new htmlWebpackPlugin({
        title: page.title,
        filename: `${page.pageId}.html`,
        template: page.htmlPath,
        hash: true,
        chunks: [page.pageId],
        meta: {
          'Content-Type': {
            'http-equiv': 'Content-Type',
            content: 'text/html; charset=utf-8'
          },
          viewport: 'width=device-width,initial-scale=1.0'
        }
      });
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name]_bundle_[hash].css'
    }),
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.css$/g,
      cssProcessor: require('cssnano'),
      cssProcessorOptions: { safe: true, discardComments: { removeAll: true } },
      canPrint: true
    })
  ],
  module: {
    rules: [
      {
        test: /.(css|scss)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [require('autoprefixer')]
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /.(jpg|png|gif|svg)$/,
        use: ['file-loader']
      },
      {
        test: /\.ts?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/, //排除掉node_module目录
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/plugin-transform-runtime'
            ]
          }
        }
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  }
};
