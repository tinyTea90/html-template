module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true
    // jquery: true
  },
  parserOptions: {
    sourceType: 'module'
  },
  extends: ['eslint:recommended'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,

        trailingComma: 'none'
      }
    ]
  }
};
