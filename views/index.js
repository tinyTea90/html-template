const path = require('path');

module.exports = [
  {
    title: '首页',
    pageId: 'home',
    htmlPath: path.join(__dirname, './home/index.html'),
    entryPath: path.join(__dirname, './home/index.ts')
  },
  {
    title: '测试页',
    pageId: 'test',
    htmlPath: path.join(__dirname, './test/index.html'),
    entryPath: path.join(__dirname, './test/index.js')
  }
];
