import request from './../../utils/request';

window.onload = function () {
  document.getElementById('postData').addEventListener('click', () => {
    request({
      url: '/helloPost',
      method: 'POST',
      data: {
        data: 'post req'
      }
    }).then((data) => {
      alert(data.data);
    });
  });

  document.getElementById('getData1').addEventListener('click', () => {
    request({
      url: '/helloGet1',
      method: 'GET'
    }).then((data) => {
      alert(data.data);
    });
  });

  document.getElementById('getData2Fail').addEventListener('click', () => {
    getData2('fail');
  });

  document.getElementById('getData2Success').addEventListener('click', () => {
    getData2('success');
  });
};

function getData2(data) {
  request({
    url: '/helloGet2',
    method: 'GET',
    params: {
      data
    }
  }).then((data) => {
    alert(data.data);
  });
}
