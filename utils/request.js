/*
 * @Author: 陈铭
 * @Date: 2020-09-22 10:40:54
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-09-22 11:06:52
 */
import Axios from 'axios';

const service = Axios.create({
  baseURL: process.env.BASE_API,
  withCredentials: true,
  timeout: 20000
});

service.interceptors.response.use(
  (response) => {
    const { status, statusText, data } = response;
    if (status === 200) {
      return data;
    } else {
      return Promise.reject(new Error(`error status: ${statusText}`));
    }
  },
  (error) => {
    return Promise.reject(new Error(`request error: ${error}`));
  }
);

export default service;
