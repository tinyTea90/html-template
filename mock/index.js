/*
 * @Author: 陈铭
 * @Date: 2020-09-22 10:25:05
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-09-22 11:14:07
 */
const delay = require('webpack-api-mocker/utils/delay');

module.exports = delay(
  {
    'POST /mock/helloPost': (req, res) => {
      const { data } = req.body; // 获取请求信息
      console.log('post: ', data);

      return res.json({
        data: 'success helloPost'
      });
    },
    'GET /mock/helloGet1': {
      data: 'success helloGet1'
    },
    'GET /mock/helloGet2': (req, res) => {
      const { data } = req.query;
      if (data === 'fail') {
        return res.status(400).json({
          status: 'error',
          code: 400
        });
      }
      return res.json({
        data: 'success helloGet2'
      });
    }
  },
  1200
);
