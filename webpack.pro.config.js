const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const webpackBase = require('./webpack.config');

console.log('production enviroment, data from database');

module.exports = webpackMerge.merge(webpackBase, {
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
      'process.env.BASE_API': JSON.stringify('/')
    })
  ]
});
